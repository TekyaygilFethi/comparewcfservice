﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace WCFSOAP
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "CompareService" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select CompareService.svc or CompareService.svc.cs at the Solution Explorer and start debugging.
    public class CompareService : ICompareService
    {
        public string Compare(int pcGuessInt, int userGuessInt)
        {
            var pcGuessTemp = pcGuessInt.ToString().ToCharArray();
            var userGuessTemp = userGuessInt.ToString().ToCharArray();
            
            int[] pcGuess = new int[pcGuessTemp.Length];
            int[] userGuess = new int[userGuessTemp.Length];

            for(int i=0;i<pcGuess.Length;i++)
            {
                pcGuess[i] = int.Parse(pcGuessTemp[i].ToString());
            }

            for (int i = 0; i < userGuess.Length; i++)
            {
                userGuess[i] = int.Parse(userGuessTemp[i].ToString());
            }
            
            int counterPositive = 0;
            int counterNegative = 0;
            char positiveSign = '*';
            char negativeSign = '*';
            string returnVal = "";

            for (int i = 0; i < pcGuess.Length; i++)
            {
                if (pcGuess.Contains(userGuess[i]))
                {

                    if (pcGuess[i] == userGuess[i])
                    {
                        counterPositive++;
                        positiveSign = '+';
                    }
                    else
                    {
                        counterNegative++;
                        negativeSign = '-';
                    }
                }
            }

            if (counterPositive > 0)
            {
                returnVal += (positiveSign.ToString() + counterPositive.ToString() + " ");
            }

            if (counterNegative > 0)
            {
                returnVal += (negativeSign.ToString() + counterNegative.ToString() + " ");
            }

            if (counterNegative == 0 && counterPositive == 0)
            {
                returnVal += "None of your numbers are matching!";
            }

            return returnVal;
        }
    }
}


